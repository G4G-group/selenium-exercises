import unittest

from test_check_environment import get_chrome_driver


class TestButtonIndex(unittest.TestCase):

    def setUp(self):
        self.driver = get_chrome_driver()

    def tearDown(self):
        self.driver.quit()

    def test_first_excercise__index1_button_should_redirect_to_first_page(self):
        LINK_TEXT = "Index 1"
        expected_url = "https://g4g-group.gitlab.io/html_css/index1.html"
        # driver.get(strona html practice)
        self.driver.get('https://g4g-group.gitlab.io/html_css/')
        # find menu button First excercise
        elem = self.driver.find_element_by_link_text(LINK_TEXT)
        # click on the button
        elem.click()
        # check / assert if page title is .../fist_page.html
        self.assertEqual('https://g4g-group.gitlab.io/html_css/index1.html', expected_url)

    def test__home_button_should_redirect_to_home_page(self):
        button_text = "Home"
        expected_url = "https://g4g-group.gitlab.io/html_css/index.html"
        # driver.get(strona html practice)
        self.driver.get('https://g4g-group.gitlab.io/html_css/')
        # find menu button First excercise
        home = self.driver.find_element_by_link_text(button_text)
        # click on the button
        home.click()
        # check / assert if page title is .../fist_page.html
        self.assertEqual('https://g4g-group.gitlab.io/html_css/index.html', expected_url)

    def test__index2_button_should_redirect_to_html_page(self):
        button_text = "Index 2"
        expected_url = "https://g4g-group.gitlab.io/html_css/index2.html"
        # driver.get(strona html practice)
        self.driver.get('https://g4g-group.gitlab.io/html_css/')
        # find menu button First excercise
        index_but = self.driver.find_element_by_link_text(button_text)
        # click on the button
        index_but.click()
        # check / assert if page title is .../fist_page.html
        self.assertEqual('https://g4g-group.gitlab.io/html_css/index2.html', expected_url)


if __name__ == '__main__':
    unittest.main()
