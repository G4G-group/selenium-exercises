import unittest
from test_check_environment import get_chrome_driver


class TestContactForm(unittest.TestCase):

    def setUp(self):
        self.driver = get_chrome_driver()

    def tearDown(self):
        self.driver.quit()

    def test_sidebar_chceckout_hefr_exercise1_redurect_to_another_page(self):
        self.driver.get('https://g4g-group.gitlab.io/html_css/')
        self.driver.implicitly_wait(20)
        first_name_field = self.driver.find_element_by_name('firstname')
        first_name_field.click()
        first_name_field.send_keys("Ann")
        return_text = first_name_field.text
        element_attribute_value = first_name_field.get_attribute ('value')

        self.assertEqual("Ann",  element_attribute_value)
