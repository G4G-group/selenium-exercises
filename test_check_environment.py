import unittest
from selenium import webdriver
import os
from selenium.webdriver.chrome.options import Options

IS_RUN_ON_CI = os.environ.get("CI_JOB_ID", False)


def get_chrome_driver():
    if IS_RUN_ON_CI:
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        return webdriver.Chrome(options=chrome_options)
    else:
        return webdriver.Chrome(executable_path=r"C:\Users\Kijki\Desktop\Sylwia\python\chromedriver.exe")


class TestCheckEnvironment(unittest.TestCase):

    def setUp(self):
        self.driver = get_chrome_driver()

    def tearDown(self):
        self.driver.quit()

    def test_google_has_title_Google(self):
        self.driver.get('https://www.google.com/')
        self.assertEqual('Google', self.driver.title)


if __name__ == '__main__':
    unittest.main()
